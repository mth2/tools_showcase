import numpy as np
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold, ParameterGrid, train_test_split

np.random.seed(123)
OPTUNA = True

data = datasets.load_digits()
X, y = data["data"], data["target"]
N = X.shape[0]
print(f"Datamatrix size {X.shape}. Label size {y.shape}")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)
print(f"Train shape: {X_train.shape}, {y_train.shape}")
print(f"Test shape: {X_test.shape}, {y_test.shape}")

if not OPTUNA:
    kf = KFold(n_splits=5)

    N_ESTIMATORS = [1, 5, 10, 20, 50, 100, 200]
    MAX_DEPTH = [1, 5, 10, 20, 100]

    kf = KFold(n_splits=5)

    # we are going to do a full grid search
    params = ParameterGrid({"n_estimators": N_ESTIMATORS, "max_depth": MAX_DEPTH})

    scores = []
    for p in params:
        c = RandomForestClassifier(**p)
        scores.append([])
        for train_index, val_index in kf.split(X_train):
            x_t, x_v = X_train[train_index], X_train[val_index]
            y_t, y_v = y_train[train_index], y_train[val_index]

            c.fit(x_t, y_t)

            preds = c.predict(x_v)

            acc = accuracy_score(y_v, preds)

            scores[-1].append(acc)

    scores_mean = [np.mean(s) for s in scores]
    scores_std = [np.std(s) for s in scores]

    idx = np.argmax(scores_mean)
    print(f"Best parameter combination: {params[idx]}")
    classifier = RandomForestClassifier(**params[idx])
    classifier.fit(X_train, y_train)

    preds = classifier.predict(X_test)
    final_acc = accuracy_score(y_test, preds)
    print(f"Final score to report: {final_acc}")

##################### OPTUNA ALTERNATIVE#####################
else:
    import optuna

    def objective(trial):

        kf = KFold(n_splits=5)

        # curent trial picks a value in the given interval, and discretization step size
        step_size_ne = 10
        step_size_md = 5

        n_e = trial.suggest_discrete_uniform("n_e", low=10, high=200, q=step_size_ne)
        m_d = trial.suggest_discrete_uniform("m_d", 1, 40, q=step_size_md)

        # NOTE even though we get discrete steps, we still need to convert to ints.....
        c = RandomForestClassifier(n_estimators=int(n_e), max_depth=int(m_d))

        scores = []
        for train_index, val_index in kf.split(X_train):
            x_t, x_v = X_train[train_index], X_train[val_index]
            y_t, y_v = y_train[train_index], y_train[val_index]

            c.fit(x_t, y_t)

            preds = c.predict(x_v)

            acc = accuracy_score(y_v, preds)

            scores.append(acc)

        # NOTE optuna minimizes by default, so for accuracy score, which we want to maximize, take the negative
        val_acc = -np.mean(scores)

        return val_acc

    # call the optimizer on the objective function
    study = optuna.create_study()
    study.optimize(objective, n_trials=10)
    print("BEST PARAMS: ", study.best_params)
