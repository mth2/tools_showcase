# tools_showcase

Small showcase of Optuna and Hydra

Optuna is used for hyper-parameter optimization in settings where grid-search is not feasible. The example here have two training loops, one with and one without optuna. 

Hydra is used for logging experiments and making them reproducible. The example here is almost too simple, but this just highlights how simple it is to actually work with Hydra. 

