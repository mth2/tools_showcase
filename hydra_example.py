"""
Adapted from
https://github.com/Jackson-Kang/Pytorch-VAE-tutorial/blob/master/01_Variational_AutoEncoder.ipynb

A simple implementation of Gaussian MLP Encoder and Decoder trained on MNIST
"""
import logging
import os
import pickle


import hydra
from omegaconf import DictConfig, OmegaConf


log = logging.getLogger()


@hydra.main(config_path="./", config_name="config")
def my_app(cfg: DictConfig) -> None:
    (OmegaConf.to_yaml(cfg))

    # "Hyperparameters"
    loops = cfg.loops
    start_val = cfg.start_val

    # NOTE
    log.info(
        "NOTE: current directory gets changed to the current hydra folder:", os.getcwd()
    )

    list = []

    for i in range(loops):
        list.append(start_val)
        log.info(f"Loop iteration {i}, current val: {start_val}")
        start_val += 1

    # Outputting file, to see how all outputs are is saved in the same folder

    with open("list.pkl", "wb") as handle:
        pickle.dump(list, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    my_app()
